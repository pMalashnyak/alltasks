package com.epam.Malashnyak.IONIOTask.DifferentReaders;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ReadersTest {
  InputStream is = new FileInputStream("temp\\data.txt");
  BufferedInputStream bis = new BufferedInputStream(is);

  public ReadersTest() throws FileNotFoundException {}

  public void read() throws IOException {
    long time = System.currentTimeMillis();
    while (is.available() > 0) {
      is.read();
    }

    System.out.println("Reader done in " + (System.currentTimeMillis() - time) + " milliseconds");
    time = System.currentTimeMillis();
    while (bis.available() > 0) {
      bis.read();
    }
    System.out.println(
        "BufferedReader done in " + (System.currentTimeMillis() - time) + " milliseconds");
  }

  public void diffBuffSize() throws IOException {
    List<BufferedInputStream> bisl = new ArrayList<BufferedInputStream>();
    for (int i = 0; i < 10; i++) {
      bisl.add(new BufferedInputStream(is, (i + 1) * 10));
    }
    int i = 1;
    for (BufferedInputStream bs : bisl) {
      long time = System.currentTimeMillis();
      while (bs.available() > 0) {
        bs.read();
      }
      System.out.println(
          "BufferedReader with buffer size of "
              + i * 10
              + " done in "
              + (System.currentTimeMillis() - time)
              + " milliseconds");
      ++i;
    }
  }
}
