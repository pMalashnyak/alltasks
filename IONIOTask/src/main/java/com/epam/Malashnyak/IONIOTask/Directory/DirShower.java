package com.epam.Malashnyak.IONIOTask.Directory;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class DirShower {
  File file;

  public DirShower(String path) {
    file = new File(path);
    ArrayList<String> names = new ArrayList<String>(Arrays.asList(file.list()));
    for (String fileName : names) {
      System.out.println(fileName);
    }
  }
}
