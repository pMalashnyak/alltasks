package com.epam.Malashnyak.IONIOTask;

import com.epam.Malashnyak.IONIOTask.DifferentReaders.ReadersTest;
import com.epam.Malashnyak.IONIOTask.Directory.DirShower;
import com.epam.Malashnyak.IONIOTask.NIOTasks.SomeBuffer;
import com.epam.Malashnyak.IONIOTask.ReadJava.ReadJava;
/*
test
 */
import java.io.*;
/*
tes2
dd
 */
public class Main {
  public static void main(String[] args) throws IOException {
    //    //task 2
    //    SerializableClass scl = new SerializableClass("someStr", 1, "serializable");
    //    try {
    //      ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("temp\\sear.dat"));
    //      out.writeObject(scl);
    //      out.close();
    //    } catch (IOException e) {
    //      e.printStackTrace();
    //    }
    //
    //    //task 3
    //    System.out.println("Comparing Readers");
    //    ReadersTest rt = new ReadersTest();
    //    rt.read();
    //    System.out.println("Comparing BufferedReaders");
    //    rt.diffBuffSize();

    // task 5
    System.out.println("Comments");
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    System.out.println("Enter file path");
    String path = br.readLine();
    ReadJava rd = new ReadJava("src\\main\\java\\com\\epam\\Malashnyak\\IONIOTask\\Main.java");

    // task 6
    // DirShower ds=new DirShower("C:\\");

    // task 7
    SomeBuffer sb = new SomeBuffer("temp\\out.txt", "temp\\in.txt");
    sb.run();
  }
}
