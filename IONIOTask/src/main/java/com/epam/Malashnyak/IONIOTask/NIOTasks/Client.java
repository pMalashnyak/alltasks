package com.epam.Malashnyak.IONIOTask.NIOTasks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class Client {
  private String name;
  private InetSocketAddress address;
  private SocketChannel client;
  private BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
  private Socket socket;

  public Client(String _name) throws IOException, InterruptedException {
    name = _name;
    address = new InetSocketAddress("localhost", 1111);
    client = SocketChannel.open(address);
    socket = new Socket("localhost", 1111);
    System.out.println("Connecting to Server on port 1111...");

    while (client.isConnected()) {
      menu();
    }
  }

  public void write() throws IOException, InterruptedException {

    System.out.println("To who:");
    String rName = br.readLine();
    System.out.println("Write message:");
    byte[] message = new String(rName + ":" + br.readLine()).getBytes();
    ByteBuffer buffer = ByteBuffer.wrap(message);
    client.write(buffer);
    buffer.clear();
    Thread.sleep(2000);
  }

  private void read() throws IOException {
    ByteBuffer bb1 = ByteBuffer.allocate(10000);
    byte[] array1 = new byte[bb1.limit()];
    InputStream in = socket.getInputStream();
    in.read(array1);
    System.out.println(array1.toString());
  }

  private void menu() throws IOException, InterruptedException {
    System.out.println("Choose");
    System.out.println("1-Write to Other User");
    System.out.println("2-Check yours messages");
    System.out.println("q-Quit");

    switch (br.readLine()) {
      case "1":
        write();
        break;
      case "2":
        read();
        break;
      case "q":
        client.close();
    }
  }
}
