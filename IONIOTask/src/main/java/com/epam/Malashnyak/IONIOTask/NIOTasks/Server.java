package com.epam.Malashnyak.IONIOTask.NIOTasks;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.*;

public class Server {
  private static List<String> messages = new ArrayList<>();
  private static Selector selector;

  private static void broadcast(String msg) throws IOException {
    ByteBuffer msgBuf = ByteBuffer.wrap(msg.getBytes());
    for (SelectionKey key : selector.keys()) {
      if (key.isValid() && key.channel() instanceof SocketChannel) {
        SocketChannel sch = (SocketChannel) key.channel();
        sch.write(msgBuf);
        msgBuf.rewind();
      }
    }
  }

  public static void main(String[] args) throws IOException {
    selector = Selector.open();

    ServerSocketChannel socket = ServerSocketChannel.open();
    InetSocketAddress address = new InetSocketAddress("localhost", 1111);

    socket.bind(address);
    socket.configureBlocking(false);

    int ops = socket.validOps();
    SelectionKey selectionKey = socket.register(selector, ops, null);

    while (true) {

      System.out.println("Server waiting");
      selector.select();

      Set<SelectionKey> keys = selector.selectedKeys();
      Iterator<SelectionKey> iterator = keys.iterator();

      while (iterator.hasNext()) {
        SelectionKey myKey = iterator.next();

        if (myKey.isAcceptable()) {
          SocketChannel client = socket.accept();

          client.configureBlocking(false);

          client.register(selector, SelectionKey.OP_READ);

          System.out.println("Connection Accepted: " + client.getLocalAddress() + "\n");

        } else if (myKey.isReadable()) {

          SocketChannel client = (SocketChannel) myKey.channel();
          ByteBuffer buffer = ByteBuffer.allocate(256);
          client.read(buffer);
          String result = new String(buffer.array()).trim();
          System.out.println("Message received: " + result);
          broadcast(result);
        }
        iterator.remove();
      }
    }
  }
}
