package com.epam.Malashnyak.IONIOTask.NIOTasks;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

public class SomeBuffer {
  private WritableByteChannel wByteChannel;
  private ReadableByteChannel rByteChannel;
  private ByteBuffer buffer = ByteBuffer.allocateDirect(10);

  public SomeBuffer(String wPath, String rPath) throws FileNotFoundException {
    wByteChannel = new FileOutputStream(wPath).getChannel();
    rByteChannel = new FileInputStream(rPath).getChannel();
  }

  public void run() throws IOException {
    int read;
    while ((read = rByteChannel.read(buffer)) > 0) {
      buffer.rewind();
      buffer.limit(read);
      while (read > 0) {
        read -= wByteChannel.write(buffer);
      }
      buffer.clear();
    }
  }
}
