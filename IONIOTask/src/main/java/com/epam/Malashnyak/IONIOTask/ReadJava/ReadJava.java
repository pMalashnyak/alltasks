package com.epam.Malashnyak.IONIOTask.ReadJava;

import java.io.*;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class ReadJava {
  String path;
  BufferedReader br;

  public ReadJava(String _path) {
    try {
      InputStream is = new FileInputStream(_path);
      BufferedInputStream bis = new BufferedInputStream(is);
      br = new BufferedReader(new InputStreamReader(bis, StandardCharsets.UTF_8));
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    try {
      read();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void read() throws IOException {
    String line;
    StringBuffer sb = new StringBuffer();
    while ((line = br.readLine()) != null) {

      if (line.length() == 0) {
        continue;
      }
      String str = findSimple(line);
      if (str != null) {
        System.out.println(str);
      }
      sb.append(line + "\n");
    }
    String context = sb.toString();
    String complComment = findComplex(context);
    while (complComment != null) {
      System.out.println(complComment);
      context = context.replace(complComment, "");
      complComment = findComplex(context);
    }
  }

  private String findSimple(String string) {
    int index = string.indexOf("//", 0);
    if (index == -1) {
      return null;
    }
    return (String) string.subSequence(index, string.length());
  }

  private String findComplex(String string) {
    int start = string.indexOf("/*", 0);
    int end = string.indexOf("*/", 0);
    if (start == -1 || end == -1) {
      return null;
    }
    return (String) string.subSequence(start, end + 2);
  }
}
