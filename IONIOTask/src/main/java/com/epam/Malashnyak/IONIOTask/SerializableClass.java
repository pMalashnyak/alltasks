package com.epam.Malashnyak.IONIOTask;

import java.io.Serializable;

public class SerializableClass implements Serializable {
  private String str;
  private int num;
  private String info;

  public SerializableClass(String _str, int _num, String _info) {
    str = _str;
    num = _num;
    info = _info;
  }
}
