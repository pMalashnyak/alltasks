package com.Malashnyak.JSON.JSONParser;

import com.Malashnyak.JSON.Models.Sweet;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Parser {
  private Gson gson;

  public Parser() {
    gson = new Gson();
  }

  public List<Sweet> getSweets(File jsonFile, File schema) throws IOException, ProcessingException {
    List<Sweet> sweets = new ArrayList<Sweet>();
    if (Validator.validate(jsonFile, schema)) {
      Type listType = new TypeToken<ArrayList<Sweet>>() {}.getType();
      sweets = gson.fromJson(new FileReader(jsonFile), listType);
      return sweets;
    } else {
      System.out.println("Invalid JSON file.");
    }
    return null;
  }
}
