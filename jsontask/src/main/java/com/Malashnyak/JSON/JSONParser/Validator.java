package com.Malashnyak.JSON.JSONParser;

import com.github.fge.jackson.JsonLoader;
import com.fasterxml.jackson.databind.*;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;

import java.io.File;
import java.io.IOException;

public class Validator {
  public static boolean validate(File json, File schemaJson)
      throws IOException, ProcessingException {
    JsonNode data = JsonLoader.fromFile(json);
    JsonNode schema = JsonLoader.fromFile(schemaJson);
    JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
    JsonValidator validator = factory.getValidator();
    ProcessingReport report = validator.validate(schema, data);
    return report.isSuccess();
  }
}
