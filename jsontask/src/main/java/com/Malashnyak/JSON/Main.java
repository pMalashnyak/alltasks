package com.Malashnyak.JSON;

import com.Malashnyak.JSON.JSONParser.Parser;
import com.Malashnyak.JSON.Models.Sweet;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class Main {
  public static void main(String[] args) {
    File json = new File("src/main/resources/sweetsJSON");
    File schema = new File("src/main/resources/sweetsJSONShema");
    Parser parser = new Parser();
    List<Sweet> sweets = new ArrayList<Sweet>();
    try {
      sweets = parser.getSweets(json, schema);
    } catch (ProcessingException |IOException e) {
      e.printStackTrace();
    }
    for (Sweet sw : sweets) {
      System.out.println(sw.toString());
    }
  }
}
