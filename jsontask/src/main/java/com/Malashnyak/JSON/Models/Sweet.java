package com.Malashnyak.JSON.Models;

import java.util.ArrayList;
import java.util.List;

public class Sweet {
  private String name;
  private String type;
  private int energy;
  private List<String> ingredients = new ArrayList<String>();
  private Value value;
  private String production;

  public Sweet(
      String _name,
      String _type,
      int _energy,
      List<String> _ingredients,
      Value _value,
      String _production) {
    name = _name;
    type = _type;
    energy = _energy;
    ingredients = _ingredients;
    value = _value;
    production = _production;
  }

  public Sweet() {}

  public int getEnergy() {
    return energy;
  }

  public void setEnergy(int energy) {
    this.energy = energy;
  }

  public void setIngredients(List<String> ingredients) {
    this.ingredients = ingredients;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setProduction(String production) {
    this.production = production;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setValue(Value value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "Sweet{ Name: "
        + name
        + " Type: "
        + type
        + " KKAL: "
        + energy
        + " Ingredients: "
        + ingredients
        + " Value: "
        + value.toString()
        + " Production: "
        + production
        + "}";
  }
}
