import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class NumberSet {
    private int leftBound;
    private int rightBound;
    private int oddSum = 0;
    private int evenSum = 0;
    private int N;
    private int[] FibonacciNum;
    private double numOfOdd = 1;
    private double numOfEven = 1;
    private BufferedReader br =
        new BufferedReader(new InputStreamReader(System.in));

    public NumberSet() throws IOException {
        System.out.print("Enter range:\n");

        leftBound = Integer.parseInt(br.readLine());
        rightBound = Integer.parseInt(br.readLine());

        if (leftBound > rightBound) {
            int swap = rightBound;
            rightBound = leftBound;
            leftBound = swap;
        }
    }

    private static boolean isOdd(final int number) {
        if (number % 2 == 0) {
            return false;
        } else {
            return true;
        }
    }

    private static int writeNumbers(final int start, final int end, final boolean isOdd) {
        int sum = 0;
        if (isOdd) {
            for (int i = start; i <= end; i += 2) {
                System.out.print(i + " ");
                sum += i;
            }
        } else {
            for (int i = start; i >= end; i -= 2) {
                System.out.print(i + " ");
                sum += i;
            }
        }
        return sum;
    }

    public void printOddNum() {
        System.out.print("Odd numbers:\n");

        if (isOdd(leftBound)) {
            oddSum = writeNumbers(leftBound, rightBound, true);
        } else {
            oddSum = writeNumbers(leftBound + 1, rightBound, true);
        }
        System.out.print("\nSum of odd numbers: " + oddSum + "\n");
    }

    public void printEvenNum() {
        if (!isOdd(rightBound)) {
            evenSum = writeNumbers(rightBound, leftBound, false);
        } else {
            evenSum = writeNumbers(rightBound - 1, leftBound, false);
        }
        System.out.print("\nSum of even numbers: " + evenSum + "\n");
    }

    public void printFibonacci() throws IOException {
        System.out.print("\nEnter number of Fibonacci numbers:\n");

        N = Integer.parseInt(br.readLine());

        FibonacciNum = new int[N];

        if (isOdd(rightBound)) {
            FibonacciNum[0] = rightBound;
            FibonacciNum[1] = leftBound - 1;
        } else {
            FibonacciNum[1] = rightBound;
            FibonacciNum[0] = rightBound - 1;
        }


        for (int i = 2; i < N; ++i) {
            FibonacciNum[i] = FibonacciNum[i - 2] + FibonacciNum[i - 1];
            if (isOdd(FibonacciNum[i])) {
                numOfOdd++;
            } else {
                numOfEven++;
            }
        }

        System.out.print("\nPercentage of odd Fibonacci numbers is: "
            + numOfOdd / (double) N);
        System.out.print("\nPercentage of even Fibonacci numbers is: "
            + numOfEven / (double) N);
    }
}
