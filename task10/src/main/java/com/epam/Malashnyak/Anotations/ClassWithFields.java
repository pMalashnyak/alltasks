package com.epam.Malashnyak.Anotations;

import com.epam.Malashnyak.Anotations.CustomAnotation.*;

public class ClassWithFields {
  @MyCustomAnnotation(name = "first field", info = "some info")
  public String firstField;

  private int secondFiels = 2;

  @MyCustomAnnotation(name = "third field", firstArg = 22)
  protected double thirdField;

  public void firstMethod() {
    System.out.println("First method invoked.");
  }

  private String secondMethod() {
    return "Second method invoked.";
  }

  protected int thirdMethod(int x, int y) {
    return x + y;
  }

  public void MyMethod(String a, int[] args) {
    for (int i : args) {
      a += i + " ";
    }
    System.out.println(a);
  }

  public void MyMethod(String[] strings) {
    for (String str : strings) {
      System.out.print(str + " ");
    }
    System.out.println();
  }
}
