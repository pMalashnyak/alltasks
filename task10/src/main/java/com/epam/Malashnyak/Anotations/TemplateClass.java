package com.epam.Malashnyak.Anotations;

import java.lang.reflect.Field;
import java.util.concurrent.ConcurrentHashMap;

public class TemplateClass<T> {
  T frst;

  T getFrst() {
    return frst;
  }

  void setFrst(T setter) {
    frst = setter;
  }

  public void setF() throws NoSuchFieldException, IllegalAccessException {
    Class tcl = TemplateClass.class;
    ConcurrentHashMap<Object, Object> concHashMap = new ConcurrentHashMap<Object, Object>();
    concHashMap.put("key1", "value1");
    Field fl = tcl.getDeclaredField("frst");
    fl.setAccessible(true);
    fl.set(this, concHashMap);
  }
}
