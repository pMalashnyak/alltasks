package room;

import toys.Toy;
import types.AgeGroup;

import java.util.List;

public class Room {
    private AgeGroup.Age ageGroup;
    private int budget;
    private List<Toy> toys;

    public Room(AgeGroup.Age _ageGroup, List<Toy> _toys) {
        ageGroup = _ageGroup;
        toys = _toys;
        countBudget();
    }

    private void countBudget() {
        for (Toy toy : toys) {
            budget += toy.getPrice();
        }
    }

    public AgeGroup.Age getAgeGroup() {
        return ageGroup;
    }

    public int getBudget() {
        return budget;
    }

    public String toString() {
        String format = "";
        for (Toy toy : toys) {
            format += toys.toString() + '\n';
        }
        return format;
    }
}
