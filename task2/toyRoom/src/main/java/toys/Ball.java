package toys;

import types.BallTypes;

public class Ball extends Toy {
    private BallTypes.BallType ballType;

    public Ball(int _price, String _color, BallTypes.BallType _ballType) {
        super(_price, _color);
        ballType = _ballType;
    }

    @Override public String toString() {
        return getColor() + " " + ballType.toString();
    }
}
