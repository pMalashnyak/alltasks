package toys;

import types.CarTypes;

public class Car extends Toy {
    private CarTypes.CarType carType;

    public Car(int _price, String _color, CarTypes.CarType _carType) {
        super(_price, _color);
        carType = _carType;
    }

    @Override public String toString() {
        return carType.toString() + " " + getColor() + " " + getGroupName();
    }
}
