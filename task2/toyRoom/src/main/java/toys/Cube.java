package toys;

import types.CubeTypes;

public class Cube extends Toy {
    private CubeTypes.CubeType type;
    private int amount;

    public Cube(int _price, String _color, CubeTypes.CubeType _type, int _amount) {
        super(_price, _color);
        type = _type;
        amount = _amount;
    }

    @Override public String toString() {
        return amount + " " + getColor() + " " + getGroupName() + "s with " + type.toString();
    }
}
