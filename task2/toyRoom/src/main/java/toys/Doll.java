package toys;

import types.Genders;

public class Doll extends Toy {
    private String hairColor;
    private Genders.Gender gender;

    public Doll(int _price, String _color, String _hairColor, Genders.Gender _gender) {
        super(_price, _color);
        hairColor = _hairColor;
        gender = _gender;
    }

    @Override public String toString() {
        return getColor() + " " + gender.toString() + " " + getGroupName() + " with " + hairColor
            + " hair ";
    }
}
