public class Main {
    public static void main(String[] args) {
        try (Resource.MyResource res = new Resource.MyResource()) {
            res.someMethod();
        }
        catch(Exception e){
            System.out.print(e.getClass());
        }
    }
}
