import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class Resource {
    public static class MyResource implements AutoCloseable {

        public MyResource() {
            System.out.print("Created MyResoure object.\n");
        }

        public void someMethod() throws Exception {
            System.out.print("someMethod called.\n");
        }

        public void close() throws Exception {
            System.out.print("Closed.\n");
            throw new Exception();
        }
    }
}
