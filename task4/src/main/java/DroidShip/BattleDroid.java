package DroidShip;

public class BattleDroid extends Droids {
    public BattleDroid(String name, int hp) {
        super(name, hp);
    }

    @Override public String toString() {
        return "I'm "+getName()+".Terminate.\n";
    }
}
