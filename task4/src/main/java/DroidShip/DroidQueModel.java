package DroidShip;

public class DroidQueModel<T extends Droids> {
    T droid;
    int priority;

    public DroidQueModel(T _droid, int _priority) {
        droid = _droid;
        priority = _priority;
    }

    @Override public String toString() {
        return "Droid: " + droid.toString() + " with " + priority + "priority\n";
    }
}
