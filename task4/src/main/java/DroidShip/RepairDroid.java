package DroidShip;

public class RepairDroid extends Droids {
    private final static String task = "repair ship";

    public RepairDroid(String name, int hp) {
        super(name, hp);
    }

    @Override public String toString() {
        return "I'm " + getName() + ".My task is to " + task + ".\n";
    }
}
