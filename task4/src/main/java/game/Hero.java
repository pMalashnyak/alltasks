package game;

import game.Monster.MonsterModel;
import game.Artifact.ArtifactModel;

public class Hero {
    public static class HeroModel {
        private int stregth;

        public HeroModel() {
            stregth = 25;
        }

        public int getStreght() {
            return stregth;
        }

        public void addStrenght(ArtifactModel art) {
            if (art == null) {
                return;
            }
            stregth += art.getStrength();
        }

        public boolean fight(MonsterModel monster) {
            if (monster == null) {
                return true;
            }

            if (getStreght() >= monster.getStrenght()) {
                return true;
            } else {
                return false;
            }
        }

    }
}
