package logicalTasks;

public class ArrayWithoutDoubles {

    private String[] array = new String[20];

    public ArrayWithoutDoubles(String[] _array) {
        array = _array;
    }

    private final int findLast(String[] arr) {
        for (int i = arr.length - 1; i > 0; --i) {
            if (arr[i] != null) {
                return i;
            }
        }
        return 0;
    }

    public void filter() {
        String[] result = new String[array.length];
        int last = findLast(array);
        int k = 0;
        boolean unique;
        int counter;
        for (int i = 0; i <= last; ++i) {
            unique = true;
            counter = 0;
            for (int j = i + 1; j <= last; j++) {
                if (array[i].equals(array[j])) {
                    counter++;
                    if (counter >= 2) {
                        unique = false;
                        break;
                    }
                }
            }
            for (int j = i - 1; j >= 0; --j) {
                if (array[i].equals(array[j])) {
                    counter++;
                    if (counter >= 2) {
                        unique = false;
                        break;
                    }
                }
            }
            if (unique) {
                result[k] = array[i];
                k++;
            }
        }
        array = result;
    }

    public void print() {
        for (String str : array) {
            if (str == null) {
                break;
            }
            System.out.print(str + ' ');
        }
        System.out.println();
    }
}
