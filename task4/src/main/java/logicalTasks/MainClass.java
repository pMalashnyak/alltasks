package logicalTasks;

public class MainClass {
    public static void main(String[] args) {
        String[] first = {"a", "D", "c", "ee", "qe"};
        String[] second = {"a", "aa", "aa", "12", "D", "ee"};

        TwoArrays arrays = new TwoArrays(first, second);
        arrays.print();
        String[] resAll = arrays.getAll();

        String[] uniqueFirst = arrays.getDistinct(1);
        String[] uniqueScnd = arrays.getDistinct(2);

        System.out.print("\nElements in both: ");
        for (String i : resAll) {
            if (i == null) {
                break;
            }
            System.out.print(i + " ");
        }

        System.out.print("\nElements only in first: ");

        for (String i : uniqueFirst) {
            if (i == null) {
                break;
            }
            System.out.print(i + " ");
        }

        System.out.print("\nElements only in second: ");
        for (String i : uniqueScnd) {
            if (i == null) {
                break;
            }
            System.out.print(i + " ");
        }
        System.out.println();

        String[] arrWithoutD = {"12", "dsf", "21", "dsf", "a", "aa", "a", "a", "a", "qwerty"};
        System.out.print("\nOriginal array: ");
        ArrayWithoutDoubles arrayWithoutDoubles = new ArrayWithoutDoubles(arrWithoutD);
        arrayWithoutDoubles.print();
        System.out.print("Array without doubles: ");
        arrayWithoutDoubles.filter();
        arrayWithoutDoubles.print();


        Integer[] integers = {1, 1, 1, 2, 3, 1, 4, 4, 4, 4, 4, 4, 5, 5, 6};
        System.out.print("\nOriginal array: ");
        ArrayWithOutSeries arrayWithOutSeries = new ArrayWithOutSeries(integers);
        arrayWithOutSeries.print();
        arrayWithOutSeries.filter();
        System.out.print("Array without series: ");
        arrayWithOutSeries.print();
    }
}
