import java.util.Iterator;
import java.util.NoSuchElementException;

public class myDeque {
    public static class MyDeque<T> implements Iterator<T>, Iterable<T> {
        private T[] deque;
        private int count = 0;

        public MyDeque() {
            deque = (T[]) new Object[5];
        }

        public MyDeque(T[] arr) {
            deque = arr;
        }

        public final void addFirst(T element) {
            T[] newDeque = (T[]) new Object[(int) (deque.length * 1.5)];
            newDeque[0] = element;
            for (int i = 0; i < findLast() + 1; ++i) {
                newDeque[i + 1] = deque[i];
            }
            deque = newDeque;
        }

        public final void addLast(T element) {
            deque[findLast() + 1] = element;
        }

        public final void removeFirst() {
            T[] newDeque = (T[]) new Object[(int) (deque.length * 1.5)];
            for (int i = 1; i < findLast() + 1; i++) {
                newDeque[i - 1] = deque[i];
            }
            deque = newDeque;
        }

        public final void removeLast() {
            T[] newDeque = (T[]) new Object[(int) (deque.length * 1.5)];
            for (int i = 0; i < findLast(); ++i) {
                newDeque[i] = deque[i];
            }
            deque = newDeque;
        }

        public final boolean offerFirst(T element) {
            try {
                addFirst(element);
            } catch (Exception e) {
                return false;
            }
            return true;
        }

        public final boolean offerLast(T element) {
            try {
                addLast(element);
            } catch (Exception e) {
                return false;
            }
            return true;
        }

        private final int findLast() {
            for (int i = deque.length - 1; i > 0; --i) {
                if (deque[i] != null) {
                    return i;
                }
            }
            return 0;
        }

        public Iterator<T> iterator() {
            return this;
        }

        public boolean hasNext() {
            if (count < findLast() + 1) {
                return true;
            } else {
                return false;
            }
        }

        public T next() {
            if (count == findLast() + 1) {
                throw new NoSuchElementException();
            }
            count++;
            return deque[count - 1];
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }


    }

    public static void main(String[] args) {
        MyDeque<Integer> deque = new MyDeque<Integer>();
        deque.addFirst(12);
        deque.addLast(33);
        System.out.print(deque.offerFirst(1));
        deque.addLast(4);
        deque.offerLast(33);
        deque.removeFirst();
        deque.removeLast();
        deque.offerLast(12);
        for (Integer el : deque) {
            System.out.print(el + "  ");
        }
    }
}
