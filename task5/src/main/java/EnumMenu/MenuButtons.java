package EnumMenu;

public class MenuButtons {
  public enum Menu {
    Put(1),
    Get(2),
    ContainsKey(3),
    ContainsValue(4),
    Remove(5),
    Clear(6),
    Print(7),
    Quit(8);

    private int option;

    Menu(int _option) {
      option = _option;
    }
  }
}
