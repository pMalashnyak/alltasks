import EnumMenu.MenuButtons;
import Tree.MyTree;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
  public static void main(String[] args) throws IOException {
    MyTree<Integer, String> ht = new MyTree<>();
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    Boolean stop = true;

    while (stop) {
      show();
      System.out.println("Enter number: ");
      Integer key;
      String value;
      Integer read = Integer.valueOf(br.readLine());
      switch (read) {
        case 1:
          System.out.println("Enter key and value: ");
          key = Integer.valueOf(br.readLine());
          value = br.readLine();
          System.out.println("Node added: " + ht.put(key, value));
          break;
        case 2:
          System.out.println("Enter key: ");
          key = Integer.valueOf(br.readLine());
          System.out.println("Value get: " + ht.get(key));
        case 3:
          System.out.println("Enter key: ");
          key = Integer.valueOf(br.readLine());
          System.out.println("Is key in tree: " + ht.containsKey(key));
          break;
        case 4:
          System.out.println("Enter value: ");
          value = br.readLine();
          System.out.println("Is value in tree: " + ht.containsValue(value));
          break;
        case 5:
          System.out.println("Enter key: ");
          key = Integer.valueOf(br.readLine());
          System.out.println("Node removed: " + ht.remove(key));
          break;
        case 6:
          ht.clear();
          System.out.println("Tree cleared");
          break;
        case 7:
          System.out.println(ht.toString());
          break;
        case 8:
          stop = false;
          break;
      }
    }
  }

  public static void show() {
    int i = 1;
    for (MenuButtons.Menu option : MenuButtons.Menu.values()) {
      System.out.println(option + ": " + i);
      i++;
    }
  }
}
