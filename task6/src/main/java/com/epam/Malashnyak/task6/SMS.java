package com.epam.Malashnyak.task6;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SMS {
  public static final String ACCOUNT_SID = "AC30853bd8bf20220952d6aaf6fd27e0ed";
  public static final String AUTH_TOKEN = "a7fe55bff4ad4632e844779a0db3c973";

  public static void send(String str) {
    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    Message message =
        Message.creator(new PhoneNumber("+380993462949"), new PhoneNumber("+17142575960"), str)
            .create();
  }
}
