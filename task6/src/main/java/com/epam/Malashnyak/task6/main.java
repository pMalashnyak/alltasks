package com.epam.Malashnyak.task6;

import org.apache.logging.log4j.*;


public class main {
    private static Logger logger = LogManager.getLogger();

    public static void main(String[] args) {

        logger.debug("This is a debug message");
        logger.info("This is an info message");
        logger.warn("This is a warn message");
        logger.error("This is an error message");
        logger.fatal("This is a fatal message");
    }
}
