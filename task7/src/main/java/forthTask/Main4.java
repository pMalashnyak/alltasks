package forthTask;

import java.io.IOException;

public class Main4 {
    public static void main(String[] args) throws IOException {
        Generator generator = new Generator();
        generator.unique();
        generator.countWords();
        generator.countChars();
    }
}
