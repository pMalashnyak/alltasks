package secondTask;

public class Command {
    public interface ICommand {
        void execute(String str);
    }
}
