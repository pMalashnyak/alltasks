package secondTask;

import java.util.ArrayList;
import java.util.List;

import secondTask.Command.*;

public class Invoker {
    private List<ICommand> history = new ArrayList<ICommand>();

    public void storeAndExecute(ICommand cmnd, String str) {
        history.add((cmnd));
        cmnd.execute(str);
    }


}
