package secondTask;

import secondTask.Command.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main2 {
    public static void main(String[] args) throws IOException {
        Invoker invk = new Invoker();

        FirstCommand frst = new FirstCommand();
        SecondCommand scn = new SecondCommand();

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


        String input = br.readLine();
        String str = br.readLine();

        if (str.equals("First")) {
            invk.storeAndExecute(frst, input);
        } else if (str.equals("Second")) {
            invk.storeAndExecute(scn, input);
        }
    }
}
