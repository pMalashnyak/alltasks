package thirdTask;

import java.util.ArrayList;
import java.util.List;

public class Main3 {
    public static void main(String[] args) {
        List<Integer> list;
        list = Generator.generate1();

        list.stream()
            .forEach(x -> System.out.print(x + " "));
        System.out.println();

        Double avrg = list
            .stream()
            .mapToDouble(a -> a)
            .average()
            .getAsDouble();

        System.out.println("Average " + avrg);

        System.out.println("Max " + list
            .stream()
            .mapToInt(a -> a)
            .max().getAsInt());

        System.out.println("Min " + list
            .stream()
            .mapToInt(a -> a)
            .min().getAsInt());

        System.out.println("Sum using sum " + list
            .stream()
            .mapToInt(a -> a)
            .sum());

        System.out.println("Sum using reduce " + list
            .stream()
            .mapToInt(a -> a)
            .reduce(Integer::sum)
            .getAsInt());

        System.out.println("Sum using reduce " + list
            .stream()
            .mapToInt(a -> a)
            .reduce(Integer::sum)
            .getAsInt());

        System.out.println("Numbers bigger than average " );
        list
            .stream()
            .filter(a -> a > avrg)
            .forEach(x -> System.out.print(x + " "));
    }
}
