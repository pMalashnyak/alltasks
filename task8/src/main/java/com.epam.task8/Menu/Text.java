package com.epam.task8.Menu;

import java.util.ListResourceBundle;

public final class Text extends ListResourceBundle {

  public final Object[][] getContents() {
    return fContents;
  }

  public static final String Hello = "Hello";
  public static final String PleaseSelectAction = "PleaseSelectAction";
  public static final String English = "English";
  public static final String French = "Francais";

  private static final Object[][] fContents = {
    {Text.Hello, "Hello"},
    {Text.PleaseSelectAction, "Please select an action"},
    {Text.English, "English"},
    {Text.French, "Francais"},
  };
}
