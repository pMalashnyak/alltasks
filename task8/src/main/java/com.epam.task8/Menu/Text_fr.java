package com.epam.task8.Menu;

import java.util.ListResourceBundle;

public final class Text_fr extends ListResourceBundle {

    public final Object[][] getContents() {
        return fContents;
    }

    //No constants are defined here

    // PRIVATE
    private static final Object[][] fContents = {
            {Text.Hello, "Bonjour"},
            {Text.PleaseSelectAction, "Veuillez choisir une action"}
    };
}
