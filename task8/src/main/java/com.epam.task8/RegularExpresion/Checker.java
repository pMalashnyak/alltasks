package com.epam.task8.RegularExpresion;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Checker {
  public static boolean check(String string) {
    return Pattern.matches("(?s)^[A-Z].*[\\.]$", string);
  }

  public static String split(String string) {
    string = string.replaceAll("[the]", "t he");
    string = string.replaceAll("[you]", "y ou");
    return string;
  }

  public static String replaseVowels(String string){
      return string.replaceAll("[aeiouyAEIOUY]","_");
  }
}
