package com.epam.Malashnyak.JUNIT;

import java.io.FileReader;
import java.io.IOException;

public class SampleClass extends IProc {
  private int x;
  private int y;
  private String str;
  protected static final int CONST = 0;

  public SampleClass() {}

  public SampleClass(int _x, int _y) {
    x = _x;
    y = _y;
  }

  public SampleClass(String _str) {
    str = _str;
  }

  public int add() {
    return x + y;
  }

  public double divide() {
    return x / y;
  }

  public String appendToString(String append) {
    return str + append;
  }

  public int doSmth() {
    return 0;
  }

  public void openFile() throws IOException {
    FileReader fr = new FileReader(str);
    fr.close();
  }

  public static void main(String[] args) {
    SampleClass s = new SampleClass(0, 0);
    s.divide();
  }

  public double get(double a){
   return 0.0;
  }
}
