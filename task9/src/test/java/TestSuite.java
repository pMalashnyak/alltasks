import org.junit.platform.suite.api.IncludeClassNamePatterns;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        SampleClassTests.class,
        VoidTest.class
})
@IncludeClassNamePatterns({"^.*$"})
public class TestSuite {}
