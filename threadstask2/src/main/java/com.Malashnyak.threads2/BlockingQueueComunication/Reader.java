package com.Malashnyak.threads2.BlockingQueueComunication;

import java.util.concurrent.BlockingQueue;

public class Reader implements Runnable {
  private final BlockingQueue<String> queue;

  public Reader(BlockingQueue<String> queue) {
    this.queue = queue;
  }

  @Override
  public void run() {
    try {

      String value = queue.take();
      while (!value.equals("*")) {
        System.out.println(value);
        value = queue.take();
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
