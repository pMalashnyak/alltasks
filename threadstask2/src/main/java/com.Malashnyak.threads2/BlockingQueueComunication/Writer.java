package com.Malashnyak.threads2.BlockingQueueComunication;

import java.io.*;
import java.util.concurrent.BlockingQueue;

public class Writer implements Runnable {
  private final BlockingQueue<String> queue;

  public Writer(BlockingQueue<String> queue) {
    this.queue = queue;
  }

  public void run() {
    String thisLine;
    try {
      FileInputStream fin = new FileInputStream("input.txt");
      BufferedReader input = new BufferedReader(new InputStreamReader(fin));
      while ((thisLine = input.readLine()) != null) {
        queue.put(thisLine);
//        System.out.println(thisLine);
      }
      fin.close();
      input.close();

      queue.put("*");
    } catch (InterruptedException | IOException e) {
      e.printStackTrace();
    }
  }
}
