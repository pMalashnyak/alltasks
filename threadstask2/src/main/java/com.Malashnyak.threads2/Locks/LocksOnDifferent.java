package com.Malashnyak.threads2.Locks;

import java.util.concurrent.locks.ReentrantLock;

public class LocksOnDifferent implements Runnable {
  private Integer integer;
  private ReentrantLock lock1 = new ReentrantLock();
  private ReentrantLock lock2 = new ReentrantLock();
  private ReentrantLock lock3 = new ReentrantLock();

  public LocksOnDifferent() {
    integer = 0;
  }

  private void methodA() {
    lock1.lock();
    try {
      for (int i = 1; i < 5; i++) {
        integer++;
        System.out.printf(Thread.currentThread().getName() + " " + integer + " MethodA\n");
        Thread.sleep(100);
      }
      methodC();
    } catch (InterruptedException e) {
      e.printStackTrace();
    } finally {
      lock1.unlock();
    }
  }

  private void methodB() {
    lock2.lock();
    try {
      integer++;
      System.out.printf(Thread.currentThread().getName() + " " + integer + " MethodB\n");
      Thread.sleep(100);

    } catch (InterruptedException e) {
      e.printStackTrace();
    } finally {
      lock2.unlock();
    }
  }

  private void methodC() {
    lock3.lock();
    try {
      for (int i = 1; i < 5; i++) {
        integer++;
        System.out.printf(Thread.currentThread().getName() + " " + integer + " MethodC\n");
        Thread.sleep(100);
        methodB();
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    } finally {
      lock3.unlock();
    }
  }

  public void run() {
    methodA();
  }
}
