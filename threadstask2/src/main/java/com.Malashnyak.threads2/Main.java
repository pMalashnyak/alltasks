package com.Malashnyak.threads2;

import com.Malashnyak.threads2.BlockingQueueComunication.Reader;
import com.Malashnyak.threads2.BlockingQueueComunication.Writer;
import com.Malashnyak.threads2.Locks.Locks;
import com.Malashnyak.threads2.Locks.LocksOnDifferent;
import com.Malashnyak.threads2.MyLock.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {
  public static void main(String[] args) throws InterruptedException, IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    menu();
    String line = br.readLine();
    while (!line.equals("q")) {
      switch (line) {
        case "1":
          System.out.println("Sync on one object");
          Integer integer = 0;
          Thread t = new Thread(new Locks(integer));
          t.start();
          System.out.println("Sync on different");
          Thread th = new Thread(new LocksOnDifferent());
          th.start();
          break;
        case "2":
          BlockingQueue<String> q = new LinkedBlockingQueue<String>();
          Thread w = new Thread(new Writer(q));
          Thread r = new Thread(new Reader(q));
          w.start();
          r.start();
          w.join();
          r.join();
          break;
        case "3":
          System.out.println("Test ReadWriteLock");
          Test test = new Test();
          test.show();
          break;
        case "q":
          break;
        default:
          System.out.println("No such option");
          break;
      }
      menu();
      line = br.readLine();
    }
  }

  static void menu() {
    System.out.println("1 - Task 1");
    System.out.println("2 - Task 2");
    System.out.println("3 - Task 3");
    System.out.println("q - Quit");
  }
}
