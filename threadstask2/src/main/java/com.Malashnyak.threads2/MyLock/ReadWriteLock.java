package com.Malashnyak.threads2.MyLock;

import java.util.HashMap;
import java.util.Map;

public class ReadWriteLock {
  private Map<Thread, Integer> readingThreads = new HashMap<>();

  private int writers = 0;
  private int writeRequests = 0;

  public synchronized void lockRead() throws InterruptedException {
    Thread call = Thread.currentThread();
    while (!canGrantAccess(call)) {
      wait();
    }
    readingThreads.put(call, getAccessCount(call) + 1);
  }

  public synchronized void unlockRead() {
    Thread call = Thread.currentThread();
    int accessCount = getAccessCount(call);
    if (accessCount == 1) {
      readingThreads.remove(call);
    } else {
      readingThreads.put(call, (accessCount - 1));
    }
    notifyAll();
  }

  public synchronized void lockWrite() throws InterruptedException {
    writeRequests++;

    while (readingThreads.size() > 0 || writers > 0) {
      wait();
    }
    writeRequests--;
    writers++;
  }

  public synchronized void unlockWrite() throws InterruptedException {
    writers--;
    notifyAll();
  }

  private boolean canGrantAccess(Thread call) {
    if (writers > 0) {
      return false;
    }
    if (isReader(call)) {
      return true;
    }
    if (writeRequests > 0) {
      return false;
    }
    return true;
  }

  private boolean isReader(Thread call) {
    return readingThreads.get(call) != null;
  }

  private int getAccessCount(Thread call) {
    Integer accessCount = readingThreads.get(call);
    if (accessCount == null) {
      return 0;
    }
    return accessCount.intValue();
  }
}
