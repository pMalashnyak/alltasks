package com.epam.Malashnyak.theadsTask1;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FibonacciExecutor {
  public void execute() {
    ExecutorService service = Executors.newCachedThreadPool();
    for (int i = 0; i < 5; ++i) {
      int finalI = i;
      service.submit(
              () -> {
                System.out.println("New " + finalI + " Tread started");
                List<Integer> num = new ArrayList<Integer>();
                for (int i1 = 0; i1 < 10; i1++) {
                  num.add(addNumber(i1));
                }
                System.out.println("Tread " + finalI + " ended");
              });
    }
    service.shutdown();
  }

  private Integer addNumber(int number) {
    if (number == 0 || number == 1) {
      return 1;
    }
    return addNumber(number - 1) + addNumber(number - 2);
  }
}
