package com.epam.Malashnyak.theadsTask1;

public class PingPong {
  private static Object sync = new Object();

  public void execute() {
    Thread t1 =
        new Thread(
            () -> {
              synchronized (sync) {
                for (int i = 0; i < 50; ++i) {
                  try {
                    System.out.println("Thread 1 is asleep");
                    sync.wait();
                  } catch (InterruptedException e) {
                  }
                  System.out.println("Thread 1 is alive");
                  sync.notify();
                }
              }
            });

    Thread t2 =
        new Thread(
            () -> {
              synchronized (sync) {
                for (int i = 0; i < 50; ++i) {
                  System.out.println("Thread 2 is alive");
                  sync.notify();
                  try {
                    System.out.println("Thread 2 is asleep");
                    sync.wait();
                  } catch (InterruptedException e) {
                  }
                }
              }
            });
    t1.start();
    t2.start();
  }
}
