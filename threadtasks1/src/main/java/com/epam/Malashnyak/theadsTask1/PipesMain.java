package com.epam.Malashnyak.theadsTask1;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class PipesMain {
  public static void main(String[] args) {

    try {
      PipedOutputStream pout = new PipedOutputStream();
      PipedInputStream pin = new PipedInputStream(pout);

      RndNumWrite write = new RndNumWrite(pout);
      RndNumRead read = new RndNumRead(pin);
      write.start();
      read.start();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
