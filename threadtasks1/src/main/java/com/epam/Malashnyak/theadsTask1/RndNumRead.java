package com.epam.Malashnyak.theadsTask1;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

public class RndNumRead extends Thread {
  private DataInputStream in;

  public RndNumRead(InputStream _in) {
    in = new DataInputStream(_in);
  }

  @Override
  public void run() {
    while (true) {
      try {
        double x = in.readDouble();
        System.out.println("Thread read: " + x);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
