package com.epam.Malashnyak.theadsTask1;

import java.io.DataOutputStream;
import java.io.OutputStream;
import java.util.Random;

public class RndNumWrite extends Thread {
  private DataOutputStream out;
  private Random rnd = new Random();

  public RndNumWrite(OutputStream os) {
    out = new DataOutputStream(os);
  }

  @Override
  public void run() {
    while (true) {
      try {
        double num = rnd.nextDouble();
        out.writeDouble(num);
        System.out.println("Thread wrote: " + num);
        out.flush();
        sleep(1000);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
}
