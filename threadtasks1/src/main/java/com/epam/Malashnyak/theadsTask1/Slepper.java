package com.epam.Malashnyak.theadsTask1;

public class Slepper extends Thread {
  @Override
  public void run() {
    int rnd = (int) (Math.random() * 10);
    try {
      System.out.println("Sleep for " + rnd + " " + getName());
      sleep((long) (rnd * 1000));
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println("Awaken " + getName());
  }
}
