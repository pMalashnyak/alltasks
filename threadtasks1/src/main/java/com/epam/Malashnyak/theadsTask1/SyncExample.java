package com.epam.Malashnyak.theadsTask1;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class SyncExample {
  private int E;
  private int F;
  private int S;
  private int T;

  public SyncExample() {
    E = 0;
    first();
    second();
    third();
  }

  public SyncExample(int i) {
    F = 0;
    S = 0;
    T = 0;

    fFirst();
    sSecond();
    tThird();
  }

  private synchronized void first() {
    Thread thread =
        new Thread(
            () -> {
              for (int i = 0; i < 5; ++i) {
                E++;
                System.out.println("First thread running.");
              }
            });
    thread.start();
  }

  private synchronized void second() {
    Thread thread =
        new Thread(
            () -> {
              for (int i = 0; i < 5; ++i) {
                E++;
                System.out.println("Second thread running.");
              }
            });
    thread.start();
  }

  private synchronized void third() {
    Thread thread =
        new Thread(
            () -> {
              for (int i = 0; i < 5; ++i) {
                E++;
                System.out.println("Third thread running.");
              }
            });
    thread.start();
  }

  private synchronized void fFirst() {
    Thread thread =
        new Thread(
            () -> {
              for (int i = 0; i < 5; ++i) {
                F++;
                System.out.println("First thread running.");
              }
            });
    thread.start();
  }

  private synchronized void sSecond() {
    Thread thread =
        new Thread(
            () -> {
              for (int i = 0; i < 5; ++i) {
                S++;
                System.out.println("Second thread running.");
              }
            });
    thread.start();
  }

  private synchronized void tThird() {
    Thread thread =
        new Thread(
            () -> {
              for (int i = 0; i < 5; ++i) {
                T++;
                System.out.println("Third thread running.");
              }
            });
    thread.start();
  }
}
