package com.Malashnyak.XML.Models;

public class Value {
  private double proteins;
  private double fat;
  private double carbohydrates;

  public Value(double _proteins, double _fat, double _carbohydrates) {
    proteins = _proteins;
    fat = _fat;
    carbohydrates = _carbohydrates;
  }

  public Value(){}

  public void setCarbohydrates(double carbohydrates) {
    this.carbohydrates = carbohydrates;
  }

  public void setFat(double fat) {
    this.fat = fat;
  }

  public void setProteins(double proteins) {
    this.proteins = proteins;
  }

  @Override
  public String toString() {
    return "Proteins: " + proteins + " Fat: " + fat + " Carbohydrates: " + carbohydrates;
  }
}
