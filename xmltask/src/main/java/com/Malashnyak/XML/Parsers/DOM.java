package com.Malashnyak.XML.Parsers;

import com.Malashnyak.XML.Models.Sweet;
import com.Malashnyak.XML.Models.Value;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DOM {
  public List<Sweet> getSweets(File xml) {
    Document document = createBuilder(xml);
    document.getDocumentElement().normalize();
    List<Sweet> sweets = new ArrayList<>();
    NodeList nodeList = document.getElementsByTagName("sweet");

    for (int i = 0; i < nodeList.getLength(); ++i) {
      Sweet sw;
      Node node = nodeList.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        Element element = (Element) node;
        List<String> ings;
        sw =
            new Sweet(
                element.getElementsByTagName("name").item(0).getTextContent(),
                element.getElementsByTagName("type").item(0).getTextContent(),
                Integer.parseInt(element.getElementsByTagName("energy").item(0).getTextContent()),
                getIngredients(element.getElementsByTagName("ingredients")),
                getValue(element.getElementsByTagName("value")),
                element.getElementsByTagName("production").item(0).getTextContent());
        sweets.add(sw);
      }
    }
    return sweets;
  }

  private List<String> getIngredients(NodeList nodes) {
    List<String> ingredients = new ArrayList<>();
    if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) nodes.item(0);
      NodeList nodeList = element.getChildNodes();
      for (int i = 0; i < nodeList.getLength(); i++) {
        Node node = nodeList.item(i);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          Element el = (Element) node;
          ingredients.add(el.getTextContent());
        }
      }
    }
    return ingredients;
  }

  private Value getValue(NodeList nodes) {
    Value value = null;
    if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) nodes.item(0);
      value =
          new Value(
              Double.parseDouble(element.getElementsByTagName("proteins").item(0).getTextContent()),
              Double.parseDouble(element.getElementsByTagName("fat").item(0).getTextContent()),
              Double.parseDouble(
                  element.getElementsByTagName("carbohydrates").item(0).getTextContent()));
    }
    return value;
  }

  private Document createBuilder(File xml) {
    DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder documentBuilder;
    Document document = null;
    try {
      documentBuilder = builderFactory.newDocumentBuilder();
      document = documentBuilder.parse(xml);
    } catch (SAXException | IOException | ParserConfigurationException ex) {
      ex.printStackTrace();
    }
    return document;
  }
}
