package com.Malashnyak.XML.Parsers.SAX;

import com.Malashnyak.XML.Models.Sweet;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SAX {
  SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

  public List<Sweet> SAXGetSweets(File xml, File xsd) {
    List<Sweet> sweets = new ArrayList<>();
    try {
      saxParserFactory.setSchema(createSchema(xsd));
      SAXParser saxParser = saxParserFactory.newSAXParser();
      SAXHandler saxHandler = new SAXHandler();
      saxParser.parse(xml, saxHandler);
      sweets = saxHandler.getSweets();
    } catch (SAXException | ParserConfigurationException | IOException e) {
      e.printStackTrace();
    }
    return sweets;
  }

  private Schema createSchema(File xsd) {
    Schema schema = null;
    try {
      String lang = XMLConstants.W3C_XML_SCHEMA_NS_URI;
      SchemaFactory factory = SchemaFactory.newInstance(lang);
      schema = factory.newSchema(xsd);
    } catch (SAXException e) {
      e.printStackTrace();
    }
    return schema;
  }
}
