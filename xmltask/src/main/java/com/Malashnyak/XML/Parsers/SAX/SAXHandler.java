package com.Malashnyak.XML.Parsers.SAX;

import com.Malashnyak.XML.Models.Sweet;
import com.Malashnyak.XML.Models.Value;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SAXHandler extends DefaultHandler {
  private List<Sweet> sweets = new ArrayList<>();
  private Sweet sweet = null;
  private List<String> ingreds;
  private Value value;

  private boolean bType = false;
  private boolean bEnergy = false;
  private boolean bIngreds = false;
  private boolean bIngred = false;
  private boolean bValue = false;
  private boolean bProt = false;
  private boolean bFat = false;
  private boolean bCarb = false;
  private boolean bProd = false;

  public List<Sweet> getSweets() {
    return sweets;
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes) {
    if (qName.equalsIgnoreCase("sweet")) {
      String beerN = attributes.getValue("name");
      sweet = new Sweet();
      sweet.setName(beerN);
    } else if (qName.equalsIgnoreCase("type")) {
      bType = true;
    } else if (qName.equalsIgnoreCase("energy")) {
      bEnergy = true;
    } else if (qName.equalsIgnoreCase("ingredients")) {
      bIngreds = true;
    } else if (qName.equalsIgnoreCase("ingredient")) {
      bIngred = true;
    } else if (qName.equalsIgnoreCase("value")) {
      bValue = true;
    } else if (qName.equalsIgnoreCase("proteins")) {
      bProt = true;
    } else if (qName.equalsIgnoreCase("fat")) {
      bFat = true;
    } else if (qName.equalsIgnoreCase("carbohydrates")) {
      bCarb = true;
    } else if (qName.equalsIgnoreCase("production")) {
      bProd = true;
    }
  }

  public void endElement(String uri, String localName, String qName) {
    if (qName.equalsIgnoreCase("sweet")) {
      sweets.add(sweet);
    }
  }

  public void characters(char ch[], int start, int length) {
    if (bType) {
      sweet.setType(new String(ch, start, length));
      bType = false;
    } else if (bEnergy) {
      sweet.setEnergy(Integer.parseInt(new String(ch, start, length)));
      bEnergy = false;
    } else if (bIngreds) {
      ingreds = new ArrayList<>();
      bIngreds = false;
    } else if (bIngred) {
      ingreds.add(new String(ch, start, length));
      bIngred = false;
    } else if (bValue) {
      value = new Value();
      bValue = false;
    } else if (bProt) {
      value.setProteins(Double.parseDouble(new String(ch, start, length)));
      bProt = false;
    } else if (bFat) {
      value.setFat(Double.parseDouble(new String(ch, start, length)));
      bFat = false;
    } else if (bCarb) {
      value.setCarbohydrates(Double.parseDouble(new String(ch, start, length)));
      bCarb = false;
    } else if (bProd) {
      sweet.setIngredients(ingreds);
      sweet.setValue(value);
      sweet.setProduction(new String(ch, start, length));
      bProd = false;
    }
  }
}
