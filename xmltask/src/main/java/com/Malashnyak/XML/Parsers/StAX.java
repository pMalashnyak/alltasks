package com.Malashnyak.XML.Parsers;

import com.Malashnyak.XML.Models.Sweet;
import com.Malashnyak.XML.Models.Value;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class StAX {
  public static List<Sweet> getSweets(File xml) {
    List<Sweet> sweets = new ArrayList<>();
    Sweet sweet = null;
    Value value = null;
    List<String> ingreds = null;
    XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
    try {
      XMLEventReader xmlEventReader =
          xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
      while (xmlEventReader.hasNext()) {
        XMLEvent xmlEvent = xmlEventReader.nextEvent();
        if (xmlEvent.isStartElement()) {
          StartElement startElement = xmlEvent.asStartElement();
          String name = startElement.getName().getLocalPart();
          switch (name) {
            case "sweet":
              sweet = new Sweet();
              break;
            case "name":
              xmlEvent = xmlEventReader.nextEvent();
              assert sweet != null;
              sweet.setName(xmlEvent.asCharacters().getData());
              break;
            case "type":
              xmlEvent = xmlEventReader.nextEvent();
              assert sweet != null;
              sweet.setType(xmlEvent.asCharacters().getData());
              break;
            case "energy":
              xmlEvent = xmlEventReader.nextEvent();
              assert sweet != null;
              sweet.setEnergy(Integer.parseInt(xmlEvent.asCharacters().getData()));
              break;
            case "ingredients":
              xmlEvent = xmlEventReader.nextEvent();
              ingreds = new ArrayList<>();
              break;
            case "ingredient":
              xmlEvent = xmlEventReader.nextEvent();
              assert ingreds != null;
              ingreds.add(xmlEvent.asCharacters().getData());
              break;
            case "value":
              xmlEvent = xmlEventReader.nextEvent();
              assert sweet != null;
              value = new Value();
              break;
            case "protein":
              xmlEvent = xmlEventReader.nextEvent();
              assert value != null;
              value.setProteins(Double.parseDouble(xmlEvent.asCharacters().getData()));
              break;
            case "fat":
              xmlEvent = xmlEventReader.nextEvent();
              assert value != null;
              value.setFat(Double.parseDouble(xmlEvent.asCharacters().getData()));
              break;
            case "carbohydrates":
              xmlEvent = xmlEventReader.nextEvent();
              assert value != null;
              value.setCarbohydrates(Double.parseDouble(xmlEvent.asCharacters().getData()));
              break;
            case "production":
              xmlEvent = xmlEventReader.nextEvent();
              assert sweet != null;
              assert ingreds != null;
              sweet.setIngredients(ingreds);
              assert value != null;
              sweet.setValue(value);
              break;
          }
        }
        if (xmlEvent.isEndElement()) {
          EndElement endElement = xmlEvent.asEndElement();
          if (endElement.getName().getLocalPart().equals("sweet")) {
            sweets.add(sweet);
          }
        }
      }
    } catch (FileNotFoundException | XMLStreamException e) {

    }
    return sweets;
  }
}
